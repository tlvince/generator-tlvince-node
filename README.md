# generator-tlvince-node

[![Build Status][travis-image]][travis-url]
[![npm version][npm-image]][npm-url]
[![License][license-image]][license-url]

[travis-url]: https://travis-ci.org/tlvince/generator-tlvince-node
[travis-image]: https://img.shields.io/travis/tlvince/generator-tlvince-node.svg
[npm-url]: https://www.npmjs.com/package/generator-tlvince-node
[npm-image]: https://img.shields.io/npm/v/generator-tlvince-node.svg
[license-url]: https://opensource.org/licenses/MIT
[license-image]: https://img.shields.io/npm/l/generator-tlvince-node.svg

> Scaffold out a node module

## Author

© 2016 Tom Vincent <git@tlvince.com> (https://tlvince.com)

## License

Released under the [MIT license](http://tlvince.mit-license.org).
